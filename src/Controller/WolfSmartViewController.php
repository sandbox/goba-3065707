<?php

namespace Drupal\wolfsmart\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WolfSmartViewController extends ControllerBase {

  /**
   * A configuration object containing wolfsmart settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * HTTP Client for API calls.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a \Drupal\upgrade_status\DeprecationAnalyser.
   *
   * @param \GuzzleHttp\Client $http_client
   *   HTTP client.
   */
  public function __construct(
    Client $http_client
  ) {
    $this->config = $this->config('wolfsmart.settings');
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Build a view page with Wolf SmartSet data.
   *
   * @return array
   *   Build array.
   */
  public function content() {
    $username = $this->config->get('username');
    $password = $this->config->get('password');
    if (empty($username) || empty($password)) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Configure the username and password first.'),
      ];
    }
    $data = [
      'headers' => [
        'Accept-Language' => 'en-GB,en-US;q=0.9,en;q=0.8',
      ],
      'form_params' => [
        'grant_type' => 'password',
        'username' => $username,
        'password' => $password,
        'client_id' => 'ro.client',
        'scope' => 'offline_access openid api',
        'website_version' => '29',
      ],
    ];
    /** @var \Psr\Http\Message\ResponseInterface $response */
    $response = $this->httpClient->request('POST', 'https://www.wolf-smartset.com/portal/connect/token', $data);
    if ($response->getStatusCode()) {
      return [
        '#type' => 'markup',
        '#markup' => $response->getBody(),
      ];
    }
  }

}
